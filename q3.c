
#include <stdio.h>

int main()
{
   int a, b, c;
    printf("Enter the value of a:", a);
    scanf("%d", &a);
    printf("Enter the value of b:", b);
    scanf("%d", &b);
    printf("Before swapping value of a = %d and b = %d\n", a, b);
    c = a;
    a = b;
    b = c;
    printf("After swapping value of a = %d and b = %d", a, b);
    return 0;